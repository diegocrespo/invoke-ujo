<#
.SYNOPSIS
Invokes a specified command (search, update, or install) using the underlying package manager I.E winget for Windows, Homebrew for Macos, and the native package manager on Linux

.DESCRIPTION
This script offers a unified way to handle software commands across multiple platforms. Depending on the OS and the package manager, it tailors commands to search, update, or install software.

.PARAMETER Command
Specifies the action to be taken. Valid commands are: 'search', 'update', and 'install'.

.PARAMETER # ProgramName
Specifies the name of the software program. This parameter is optional unless the command is 'search'.

.PARAMETER Admin
A switch indicating that the command requires administrative rights. This is necessary for commands like 'update' and 'install' on some OSes. Will call sudo on Mac and Linux, and runAs on Windows

# .PARAMETER DryRun
# A switch that, when provided, will print the command that would be executed rather than executing it.

.NOTES
This script makes use of helper scripts: Update.ps1, Search.ps1, and Install.ps1.
Make sure these scripts are present in the same directory.

.EXAMPLE
.\Invoke-Ujo.ps1 -Command 'search' -ProgramName 'firefox' -Admin

Searches for the program 'firefox' with administrative rights.

.EXAMPLE
.\Invoke-Ujo.ps1 -Command 'install' -ProgramName 'vlc' -Admin -DryRun

.EXAMPLE
.\Invoke-Ujo.ps1 -Command update -Admin

Shows the install command for 'vlc' without actually executing it.

#>
param ( 
    [Parameter(Mandatory=$true, Position=0)]
    [string]$Command,
    [string]$ProgramName,
    [switch]$Admin,
    [switch]$DryRun
)

. ./Update.ps1
. ./Search.ps1
. ./Install.ps1

$argV = @{
    os = ""
    admin = ""
    packageManager = ""
}

$linuxDistros = @{
    "manjaro" = "pacman"
    "ubuntu" = "apt"
    "fedora" = "dnf"
    "debian" = "apt"
    "centos" = "yum"
    "opensuse" = "zypper"
    "arch linux" = "pacman"
    "mint" = "apt"
}

try {
    $processInfo = New-Object System.Diagnostics.ProcessStartInfo

    if ($isWindows) {
        $argV['os'] = "Windows"
        $argV['packageManager'] = "winget"
        $argV['admin'] = "runAs"
	if($Command -eq "search"){
	    SearchWindows($argV['packageManager'])
	}
    }
    elseif ($isLinux) {
        $osReleaseFile = Get-Content "/etc/os-release"
        $Os = GetOsReleaseField "ID"
        $argV['os'] = $Os
        if ($linuxDistros.ContainsKey($Os)){
            $argV['packageManager'] = $linuxDistros[$Os]
        }
        else{
            throw "Error: The key '$Os' is not present in the list of Linux distributions."
        }
        $argV['admin'] = "sudo"
        if ($Command -eq "search"){
	    if($Admin){
		$processInfo.FileName = $argV['admin']
		$searchCommand = SearchLinux($argV['packageManager'])
		$processInfo.Arguments = "$($argV['packageManager']) $searchCommand $ProgramName"
	    }
	    else{
		$processInfo.FileName = $argV['packageManager']
		$searchCommand = SearchLinux($argV['packageManager'])
		$processInfo.Arguments = "$searchCommand $ProgramName"
        }
	}
	elseif($Command -eq "update"){
	    if(-not $Admin){
		throw "Requires Admin flags to run"
	    }else
	    {
		$processInfo.FileName = $argV['admin']
		$updateCommand = UpdateLinux($argV['packageManager'])
		$processInfo.Arguments = "$($argV['packageManager']) $updateCommand $ProgramName"
	    }
	}
	elseif($Command -eq "install"){
	    if(-not $Admin){
		throw "Requires Admin flags to run"
	    }else
	    {
		$processInfo.FileName = $argV['admin']
		$updateCommand = InstallLinux($argV['packageManager'])
		$processInfo.Arguments = "$($argV['packageManager']) $updateCommand $ProgramName"
	    }
	}
    }
    elseif ($isMacOs) {
        $argV['os'] = "macOS"
        $argV['packageManager'] = "brew"
        $argV['admin'] = "sudo"
	if($Command -eq "search"){
	    $searchCommand = SearchMac($argV['packageManager'])
	    $processInfo.Arguments ="$($argV['admin'])$($argV['packageManager']) $searchCommand"
	}
    }
    else {
        throw "Error: Unknown Operating System. Aborting"
    }

    if($DryRun){
	Write-Host "command: $($processInfo.FileName) $($processInfo.Arguments)"
    }
    else{
	$processInfo.RedirectStandardOutput = $true
	$processInfo.UseShellExecute = $false

	$process = New-Object System.Diagnostics.Process
	$process.StartInfo = $processInfo
	$process.Start() | Out-Null

	# Read the output from the command and display it 
	$output = $process.StandardOutput.ReadToEnd()
	Write-Host $output
	$process.WaitForExit()
    }
}
catch {
    Write-Host "An error occurred: $($_.Exception.Message)"
}
param ( 
    [Parameter(Mandatory=$true, Position=0)]
    [string]$Command,
    [string]$ProgramName,
    [switch]$Admin,
    [switch]$DryRun
)

. ./Update.ps1
. ./Search.ps1
. ./Install.ps1

$argV = @{
    os = ""
    admin = ""
    packageManager = ""
}

$linuxDistros = @{
    "manjaro" = "pacman"
    "ubuntu" = "apt"
    "fedora" = "dnf"
    "debian" = "apt"
    "centos" = "yum"
    "opensuse" = "zypper"
    "arch linux" = "pacman"
    "mint" = "apt"
}

try {
    $processInfo = New-Object System.Diagnostics.ProcessStartInfo

    if ($isWindows) {
        $argV['os'] = "Windows"
        $argV['packageManager'] = "winget"
        $argV['admin'] = "runAs"
	if($Command -eq "search"){
	    SearchWindows($argV['packageManager'])
	}
    }
    elseif ($isLinux) {
        $osReleaseFile = Get-Content "/etc/os-release"
        $Os = GetOsReleaseField "ID"
        $argV['os'] = $Os
        if ($linuxDistros.ContainsKey($Os)){
            $argV['packageManager'] = $linuxDistros[$Os]
        }
        else{
            throw "Error: The key '$Os' is not present in the list of Linux distributions."
        }
        $argV['admin'] = "sudo"
        if ($Command -eq "search"){
	    if($Admin){
		$processInfo.FileName = $argV['admin']
		$searchCommand = SearchLinux($argV['packageManager'])
		$processInfo.Arguments = "$($argV['packageManager']) $searchCommand $ProgramName"
	    }
	    else{
		$processInfo.FileName = $argV['packageManager']
		$searchCommand = SearchLinux($argV['packageManager'])
		$processInfo.Arguments = "$searchCommand $ProgramName"
        }
	}
	elseif($Command -eq "update"){
	    if(-not $Admin){
		throw "Requires Admin flags to run"
	    }else
	    {
		$processInfo.FileName = $argV['admin']
		$updateCommand = UpdateLinux($argV['packageManager'])
		$processInfo.Arguments = "$($argV['packageManager']) $updateCommand $ProgramName"
	    }
	}
	elseif($Command -eq "install"){
	    if(-not $Admin){
		throw "Requires Admin flags to run"
	    }else
	    {
		$processInfo.FileName = $argV['admin']
		$updateCommand = InstallLinux($argV['packageManager'])
		$processInfo.Arguments = "$($argV['packageManager']) $updateCommand $ProgramName"
	    }
	}
    }
    elseif ($isMacOs) {
        $argV['os'] = "macOS"
        $argV['packageManager'] = "brew"
        $argV['admin'] = "sudo"
	if($Command -eq "search"){
	    $searchCommand = SearchMac($argV['packageManager'])
	    $processInfo.Arguments ="$($argV['admin'])$($argV['packageManager']) $searchCommand"
	}
    }
    else {
        throw "Error: Unknown Operating System. Aborting"
    }

    if($DryRun){
	Write-Host "command: $($processInfo.FileName) $($processInfo.Arguments)"
    }
    else{
	$processInfo.RedirectStandardOutput = $true
	$processInfo.UseShellExecute = $false

	$process = New-Object System.Diagnostics.Process
	$process.StartInfo = $processInfo
	$process.Start() | Out-Null

	# Read the output from the command and display it 
	$output = $process.StandardOutput.ReadToEnd()
	Write-Host $output
	$process.WaitForExit()
    }
}
catch {
    Write-Host "An error occurred: $($_.Exception.Message)"
}
