function UpdateLinux($arg){
    if ($arg -eq "pacman"){
	return "-Sy"
    }
    elseif ($arg -eq "apt"){
	return "update"
    }
    elseif ($arg -eq "zypper"){
	return "refresh"
    } 
    elseif ($arg -eq "dnf"){
	return "make cache"
    }
    else{
	throw "I don't know how to update using $($arg)"
    }
}
