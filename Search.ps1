# Figure out which Linux you are one
function GetOsReleaseField($field){
    foreach ($line in $osReleaseFile){
	if ($line -match "$field\s*=\s*(.+)"){
	    return $Matches[1]
	}
    }
}
    
function SearchLinux($arg){
    if ($arg -eq "pacman"){
	return "-Ss"
    }
    elseif ($arg -eq "apt"){
	return "search"
    }
    elseif ($arg -eq "zypper"){
	return "search"
    } 
    elseif ($arg -eq "dnf"){
	return "search"
    }
    else{
	throw "I don't know how to search using $($arg)"
    }
}

function SearchMac($arg){
   Write-Host "Searching Mac" 
}

function SearchWindows($arg){
    return "search"
}

