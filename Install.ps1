function InstallLinux($arg){
    if ($arg -eq "pacman"){
	return "-S"
    }
    elseif ($arg -eq "apt"){
	return "install"
    }
    elseif ($arg -eq "zypper"){
	return "install"
    } 
    elseif ($arg -eq "dnf"){
	return "install"
    }
    else{
	throw "I don't know how to install packages using $($arg)"
    }
}
